include(LibFindMacros)

libfind_pkg_check_modules(libtcod_PKGCONF tcod)

find_path(libtcod_INCLUDE_DIR
    NAMES libtcod.hpp
    PATHS ${libtcod_PKGCONF_INCLUDE_DIRS}
)

find_library(tcod_LIBRARY
    NAMES libtcod
    PATHS ${libtcod_PKGCONF_LIBRARY_DIRS}
)

libfind_process(tcod)
