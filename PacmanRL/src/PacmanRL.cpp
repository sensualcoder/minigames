#include "PacmanRL.hpp"

namespace prl
{
    static const std::string NAME { "PacmanRL" };

    PacmanRL::PacmanRL(const core::rect_t& window_size)
        : Cartridge(window_size, NAME)
    {
    }

    bool PacmanRL::Init()
    {
        return true;
    }

    void PacmanRL::Event(const core::event_t&)
    {
    }

    void PacmanRL::Update()
    {
    }

    void PacmanRL::Render(core::Renderer*)
    {
    }
} // namespace prl
