#ifndef PACMAN_RL_HPP
#define PACMAN_RL_HPP

#include "Cartridge.hpp"

namespace prl
{
    class PacmanRL final : public core::Cartridge
    {
    public:
        PacmanRL(const core::rect_t& window_size);

        bool Init() override;
        void Event(const core::event_t& event) override;
        void Update() override;
        void Render(core::Renderer* renderer) override;
    };
} // namespace prl

#endif // PACMAN_RL_HPP
