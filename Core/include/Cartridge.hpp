#ifndef CARTRIDGE_HPP
#define CARTRIDGE_HPP

#include "Rect.hpp"

#include <libtcod/console.hpp>

#include <string>

namespace core
{
    struct event_t;

    class Renderer;

    class Cartridge
    {
    public:
        Cartridge(const rect_t& window_size, const std::string& name)
            : name_(name),
              console_(std::make_unique<TCODConsole>(window_size.width, window_size.height))
        {
        }

        virtual ~Cartridge() = default;

        Cartridge(const Cartridge&) = delete;
        Cartridge(Cartridge&&) = default;

        Cartridge& operator=(const Cartridge&) = delete;
        Cartridge& operator=(Cartridge&&) = default;

        inline const std::string& GetName() const
        {
            return name_;
        }

        virtual bool Init() = 0;
        virtual void Event(const event_t& event) = 0;
        virtual void Update() = 0;
        virtual void Render(Renderer* renderer) = 0;

    protected:
        std::string name_;

        std::unique_ptr<TCODConsole> console_;
    };
} // namespace core

#endif // CARTRIDGE_HPP
