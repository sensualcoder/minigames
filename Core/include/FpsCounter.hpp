#ifndef FPS_COUNTER_HPP
#define FPS_COUNTER_HPP

#include <memory>

class TCODConsole;

namespace core
{
    class FpsCounter
    {
    public:
        FpsCounter();

        void Render(TCODConsole* parent) const;

    private:
        std::unique_ptr<TCODConsole> console_;
    };
} // namespace core

#endif // FPS_COUNTER_HPP
