#ifndef INPUT_HANDLER_HPP
#define INPUT_HANDLER_HPP

#include "Event.hpp"

#include <libtcod/sys.hpp>

namespace core
{
    class InputHandler
    {
    public:
        event_t Event();

    private:
        TCOD_key_t key_;
        TCOD_mouse_t mouse_;
    };
} // namespace core

#endif // INPUT_HANDLER_HPP
