#ifndef MAIN_MENU_HPP
#define MAIN_MENU_HPP

#include "Cartridge.hpp"
#include "Rect.hpp"

#include <functional>
#include <memory>
#include <vector>

namespace core
{
    struct event_t;

    class Renderer;

    class MainMenu : public Cartridge
    {
    public:
        MainMenu(const rect_t& window_size,
                 const std::vector<std::reference_wrapper<Cartridge>>& games);

        bool Init() override;
        void Event(const event_t& event) override;
        void Update() override;
        void Render(Renderer* renderer) override;

        Cartridge* GetGame(std::size_t index) const;

    private:
        std::vector<std::reference_wrapper<Cartridge>> games_;
    };
} // namespace core

#endif // MAIN_MENU_HPP
