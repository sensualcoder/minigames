#ifndef TILE_HPP
#define TILE_HPP

#include "Position.hpp"

namespace core
{
    struct tile_t
    {
        pos_t pos;
    };
} // namespace core

#endif // TILE_HPP
