#ifndef EVENT_HPP
#define EVENT_HPP

#include <libtcod/sys.hpp>

namespace core
{
    struct event_t
    {
        const TCOD_key_t key;
        const TCOD_mouse_t mouse;
    };
}

#endif // EVENT_HPP
