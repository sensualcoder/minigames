#ifndef GAME_MAIN_HPP
#define GAME_MAIN_HPP

#include "FpsCounter.hpp"
#include "InputHandler.hpp"
#include "MainMenu.hpp"
#include "Renderer.hpp"

#include <memory>

namespace core
{
    class Cartridge;

    class GameMain
    {
    public:
        GameMain();
        ~GameMain() = default;

        GameMain(const GameMain&) = delete;
        GameMain(GameMain&&) = delete;

        GameMain& operator=(const GameMain&) = delete;
        GameMain& operator=(GameMain&&) = delete;

        bool Init();
        void Event();
        void Update();
        void Render() const;

        int Main();

    private:
        std::unique_ptr<InputHandler> input_;
        std::unique_ptr<Renderer> renderer_;

        Cartridge* current_game_;
        
        std::unique_ptr<FpsCounter> fps_;
    };
} // namespace core

#endif // GAME_MAIN_HPP
