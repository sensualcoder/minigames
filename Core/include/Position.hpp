#ifndef POSITION_HPP
#define POSITION_HPP

namespace core
{
    struct pos_t
    {  
        int x;
        int y;
    };

    pos_t operator+(const pos_t& lhs, const pos_t& rhs);
    pos_t operator-(const pos_t& lhs, const pos_t& rhs);
    
    bool operator==(const pos_t& lhs, const pos_t& rhs);
    bool operator!=(const pos_t& lhs, const pos_t& rhs);
}

#endif // POSITION_HPP
