#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "Rect.hpp"

#include <libtcod/console.hpp>

#include <string>
#include <vector>

namespace core
{
    class Renderable;

    class Renderer
    {
    public:
        explicit Renderer(const rect_t& window_size, const std::string& title,
                          bool fullscreen, TCOD_renderer_t renderer);
        ~Renderer();

        TCODConsole* GetConsole() const;

        void Flush() const;
        bool IsWindowClosed() const;
        void Clear() const;
    };
} // namespace core

#endif // RENDERER_HPP
