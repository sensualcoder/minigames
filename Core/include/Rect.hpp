#ifndef RECT_HPP
#define RECT_HPP

namespace core
{
    struct rect_t
    {
        int width;
        int height;
    };
}

#endif // RECT_HPP
