#include "Renderer.hpp"

#include <libtcod/sys.hpp>

namespace core
{
    Renderer::Renderer(const rect_t& window_size, const std::string& title,
                       bool fullscreen, TCOD_renderer_t renderer)
    {
        TCODConsole::initRoot(window_size.width, window_size.height,
                              title.c_str(), fullscreen, renderer);

        TCODConsole::root->setDefaultBackground(TCODColor::black);
        TCODConsole::root->clear();

        TCODSystem::setFps(30);
    }

    Renderer::~Renderer()
    {
        TCOD_quit();
    }

    TCODConsole* Renderer::GetConsole() const
    {
        return TCODConsole::root;
    }

    void Renderer::Flush() const
    {
        TCODConsole::root->flush();
    }

    bool Renderer::IsWindowClosed() const
    {
        return TCODConsole::root->isWindowClosed();
    }

    void Renderer::Clear() const
    {
        TCODConsole::root->clear();
    }
} // namespace core
