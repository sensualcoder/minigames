#include "Position.hpp"

namespace core
{
    pos_t operator+(const pos_t& lhs, const pos_t& rhs)
    {
        return pos_t { lhs.x + rhs.x, lhs.y + rhs.y };
    }

    pos_t operator-(const pos_t& lhs, const pos_t& rhs)
    {
        return pos_t { lhs.x - rhs.x, lhs.y - rhs.y };
    }

    bool operator==(const pos_t& lhs, const pos_t& rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    bool operator!=(const pos_t& lhs, const pos_t& rhs)
    {
        return !(lhs == rhs);
    }
} // namespace core
