#include "GameMain.hpp"

#include "Cartridge.hpp"
#include "Event.hpp"
#include "MainMenu.hpp"
#include "Rect.hpp"
#include "TicTacToe.hpp"

#include <functional>
#include <memory>
#include <vector>

namespace core
{
    static const rect_t WINDOW_SIZE { 80, 80 };
    static const std::string TITLE = "Minigames";
    static const bool FULLSCREEN = false;
    static const TCOD_renderer_t RENDERER = TCOD_RENDERER_SDL2;

    static const std::unique_ptr<Cartridge> TTT =
        std::make_unique<ttt::TicTacToe>(WINDOW_SIZE);

    static const std::vector<std::reference_wrapper<Cartridge>> GAMES_LIST {
        *TTT
    };

    static const std::unique_ptr<Cartridge> MAIN_MENU =
        std::make_unique<MainMenu>(WINDOW_SIZE, GAMES_LIST);

    GameMain::GameMain()
        : input_(std::make_unique<InputHandler>()),
          renderer_(std::make_unique<Renderer>(WINDOW_SIZE, TITLE, FULLSCREEN,
                                               RENDERER)),
          current_game_(TTT.get()),
          fps_(std::make_unique<FpsCounter>())
    {
    }

    bool GameMain::Init()
    {
        current_game_->Init();

        return true;
    }

    void GameMain::Event()
    {
        auto event = input_->Event();

        current_game_->Event(event_t { event });
    }

    void GameMain::Update()
    {
        current_game_->Update();
    }

    void GameMain::Render() const
    {
        // Clear renderer
        renderer_->Clear();

        current_game_->Render(renderer_.get());

        fps_->Render(renderer_->GetConsole());

        // Flush renderer
        renderer_->Flush();
    }

    int GameMain::Main()
    {
        if (!this->Init())
        {
            return -1;
        }

        while (!renderer_->IsWindowClosed())
        {
            this->Event();
            this->Update();
            this->Render();
        }

        return 0;
    }
} // namespace core
