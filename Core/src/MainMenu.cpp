#include "MainMenu.hpp"

#include "Cartridge.hpp"
#include "Event.hpp"
#include "Renderer.hpp"

namespace core
{
    MainMenu::MainMenu(
        const rect_t& window_size,
        const std::vector<std::reference_wrapper<Cartridge>>& games)
        : Cartridge(window_size, "Main Menu"),
          games_(games)
    {
    }

    bool MainMenu::Init()
    {
        return true;
    }

    void MainMenu::Event(const event_t& event)
    {
    }

    void MainMenu::Update()
    {
        // Noop
    }

    void MainMenu::Render(Renderer* renderer)
    {
        for (std::size_t i = 0; i < games_.size(); ++i)
        {
        }
    }

    Cartridge* MainMenu::GetGame(std::size_t index) const
    {
        if (index > games_.size())
        {
            return nullptr;
        }

        return &games_[index].get();
    }
} // namespace core
