#include "FpsCounter.hpp"

#include "Rect.hpp"

#include <libtcod/console.hpp>
#include <libtcod/sys.hpp>

namespace core
{
    static const rect_t SIZE { 9, 1 };

    FpsCounter::FpsCounter()
        : console_(std::make_unique<TCODConsole>(SIZE.width, SIZE.height))
    {
        console_->setDefaultBackground(TCODColor::azure);
        console_->flush();
    }

    void FpsCounter::Render(TCODConsole* parent) const
    {
        console_->clear();

        console_->printf(0, 0, " FPS: %i", TCODSystem::getFps());

        TCODConsole::blit(console_.get(), 0, 0, SIZE.width, SIZE.height, parent,
                          0, 0);
    }
} // namespace core
