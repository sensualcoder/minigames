#include "InputHandler.hpp"

namespace core
{
    event_t InputHandler::Event()
    {
        if (TCODSystem::checkForEvent(TCOD_EVENT_ANY, &key_, &mouse_)
            != TCOD_EVENT_NONE)
        {
            return event_t { key_, mouse_ };
        }

        return event_t {};
    }
} // namespace core
