#include "Grid.hpp"

#include <libtcod/console.hpp>

#include <unordered_map>

namespace ttt
{
    const int MIN_MOVES_FOR_WIN = 5;

    const std::unordered_map<char, winner_t> MOVE_MAP { { 'X', winner_t::X },
                                                        { 'O', winner_t::O } };

    inline constexpr int pos_to_vector(const core::pos_t& pos, int height)
    {
        return pos.x + pos.y * height;
    }

    Grid::Grid(const core::rect_t& size)
        : moves_(0),
          size_(size),
          grid_(size.width * size.height, 0),
          console_(std::make_unique<TCODConsole>(size.width, size.height))
    {
    }

    bool Grid::SetGridPosition(const core::pos_t& pos, player_t player)
    {
        int index = this->PosToVector(pos);

        if (index < 0 || std::size_t(index) >= grid_.size())
        {
            return false;
        }

        if (grid_[index] != 0)
        {
            return false;
        }

        switch (player)
        {
            case (player_t::PLAYER_O):
                grid_[index] = 'O';
                break;
            case (player_t::PLAYER_X):
                grid_[index] = 'X';
                break;
            default:
                break;
        }

        ++moves_;

        return true;
    }

    winner_t Grid::CheckForWin() const
    {
        // If less than four moves, just return none
        if (moves_ < MIN_MOVES_FOR_WIN)
        {
            return winner_t::NONE;
        }

        for (int i = 0; i < size_.width; ++i)
        {
            auto move = grid_[this->PosToVector(core::pos_t { i, 0 })];

            if (i == 0)
            {
                // Check rows
                for (int j = i + 1; j < size_.width; ++j)
                {
                    auto check = grid_[this->PosToVector(core::pos_t { j, 0 })];

                    if (check != move)
                    {
                        break;
                    }

                    if (j + 1 == size_.width)
                    {
                        try
                        {
                            return MOVE_MAP.at(check);
                        }
                        catch (std::exception&)
                        {
                            // Not in map
                        }
                    }
                }

                // Check columns
                for (int j = i + 1; j < size_.height; ++j)
                {
                    auto check = grid_[this->PosToVector(core::pos_t { i, j })];

                    if (check != move)
                    {
                        break;
                    }

                    if (j + 1 == size_.width)
                    {
                        try
                        {
                            return MOVE_MAP.at(check);
                        }
                        catch (std::exception&)
                        {
                            // Not in map
                        }
                    }
                }

                // Check diagonals
            }
            else if (i == size_.width - 1)
            {
                // Check diagonal
            }
            else
            {
                // Check columns
            }
        }

        // If no one wins and grid is full, then tie
        if (moves_ == grid_.size())
        {
            return winner_t::TIE;
        }

        // Otherwise, no winner
        return winner_t::NONE;
    }

    void Grid::Render(TCODConsole* console) const
    {
        for (int x = 0; x < size_.width; ++x)
        {
            for (int y = 0; y < size_.height; ++y)
            {
                auto move =
                    grid_[pos_to_vector(core::pos_t { x, y }, size_.height)];

                console->putChar(x, y, move);
            }
        }
    }

    int Grid::PosToVector(const core::pos_t& pos) const
    {
        return pos_to_vector(pos, size_.height);
    }
} // namespace ttt
