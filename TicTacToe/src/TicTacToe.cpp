#include "TicTacToe.hpp"

#include "Cartridge.hpp"
#include "Event.hpp"
#include "Rect.hpp"

#include <string>

namespace ttt
{
    static const std::string NAME { "Tic-Tac-Toe" };
    static const core::rect_t GRID_SIZE { 3, 3 };
    static const core::rect_t BOARD_SIZE { 5, 5 };

    TicTacToe::TicTacToe(const core::rect_t& window_size)
        : core::Cartridge(window_size, NAME), grid_(GRID_SIZE), players_(2)
    {
    }

    bool TicTacToe::Init()
    {
        for (int x = 0; x < BOARD_SIZE.width; ++x)
        {
            for (int y = 0; y < BOARD_SIZE.height; ++y)
            {
                if (y == 1 || y == 3)
                {
                    console_->setCharBackground(
                        x + console_->getWidth() / 2 - GRID_SIZE.width / 2,
                        y + console_->getHeight() / 2 - GRID_SIZE.height / 2,
                        TCODColor::grey);
                }
                else if (x == 1 || x == 3)
                {
                    console_->setCharBackground(
                        x + console_->getWidth() / 2 - GRID_SIZE.width / 2,
                        y + console_->getHeight() / 2 - GRID_SIZE.height / 2,
                        TCODColor::grey);
                }
            }
        }

        players_.write(player_t::PLAYER_X);
        players_.write(player_t::PLAYER_O);

        return true;
    }

    void TicTacToe::Event(const core::event_t& event)
    {
        if (event.mouse.lbutton)
        {
            if (grid_.SetGridPosition(
                    core::pos_t { event.mouse.x, event.mouse.y },
                    players_.read()))
            {
                players_.next();
            }
        }
    }

    void TicTacToe::Update()
    {
    }

    void TicTacToe::Render(core::Renderer* renderer)
    {
        grid_.Render(console_.get());

        auto* parent = renderer->GetConsole();

        TCODConsole::blit(console_.get(), 0, 0, console_->getWidth(),
                          console_->getHeight(), parent, 0, 0);
    }
} // namespace ttt
