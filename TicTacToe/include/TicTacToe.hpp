#ifndef TIC_TAC_TOE_HPP
#define TIC_TAC_TOE_HPP

#include "Cartridge.hpp"
#include "Event.hpp"
#include "Grid.hpp"
#include "Rect.hpp"
#include "Renderer.hpp"
#include "RingList.hpp"

namespace ttt
{
    class TicTacToe final : public core::Cartridge
    {
    public:
        TicTacToe(const core::rect_t& window_size);
        ~TicTacToe() = default;

        TicTacToe(const TicTacToe&) = delete;
        TicTacToe(TicTacToe&&) = default;

        TicTacToe& operator=(const TicTacToe&) = delete;
        TicTacToe& operator=(TicTacToe&&) = default;

        bool Init() override;
        void Event(const core::event_t& event) override;
        void Update() override;
        void Render(core::Renderer* renderer) override;

    private:
        Grid grid_;

        util::RingList<player_t> players_;
    };
} // namespace ttt

#endif // TIC_TAC_TOE_HPP
