#ifndef PLAYER_HPP
#define PLAYER_HPP

namespace ttt
{
    enum class player_t
    {
        PLAYER_X,
        PLAYER_O
    };
}

#endif // PLAYER_HPP
