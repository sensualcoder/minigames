#ifndef WINNER_HPP
#define WINNER_HPP

namespace ttt
{
    enum class winner_t
    {
        NONE,
        TIE,
        X,
        O
    };
}

#endif // WINNER_HPP
