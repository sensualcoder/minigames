#ifndef GRID_HPP
#define GRID_HPP

#include "Player.hpp"
#include "Position.hpp"
#include "Rect.hpp"
#include "Winner.hpp"

#include <libtcod/console.hpp>

#include <memory>
#include <vector>

class TCODConsole;

namespace ttt
{
    class Grid
    {
    public:
        Grid(const core::rect_t& size);

        bool SetGridPosition(const core::pos_t& pos, player_t player);

        winner_t CheckForWin() const;
        void Render(TCODConsole* parent) const;

    private:
        int PosToVector(const core::pos_t& pos) const;

        std::size_t moves_;
        core::rect_t size_;
        std::vector<char> grid_;

        std::unique_ptr<TCODConsole> console_;
    };
} // namespace ttt

#endif // GRID_HPP
