#ifndef RING_LIST_HPP
#define RING_LIST_HPP

#include <cstdint>
#include <stdexcept>
#include <vector>

namespace util
{
    template <typename T>
    class RingList
    {
    public:
        explicit RingList(std::size_t size);

        T read();
        void next();
        void write(const T& value);
        void overwrite(const T& value);
        void remove(std::size_t index);
        void clear();

    private:
        std::size_t size_, max_size_;
        std::size_t start_, end_, cur_;
        std::vector<T> buffer_;

        void insert_or_replace(std::size_t index, const T& value);
    };

    template <typename T>
    RingList<T>::RingList(std::size_t size)
        : size_(0), max_size_(size), start_(0), end_(0), cur_(0)
    {
        buffer_.reserve(size);
    }

    template <typename T>
    T RingList<T>::read()
    {
        if (size_ == 0)
        {
            throw std::domain_error("Empty buffer");
        }

        auto value = buffer_[cur_];

        return value;
    }

    template <typename T>
    void RingList<T>::next()
    {
        cur_ == size_ ? cur_ = 0 : ++cur_;
    }

    template <typename T>
    void RingList<T>::write(const T& value)
    {
        if (size_ == max_size_)
        {
            throw std::domain_error("Buffer full");
        }

        this->insert_or_replace(end_, value);

        end_ = (end_ + 1) % max_size_;
        ++size_;
    }

    template <typename T>
    void RingList<T>::overwrite(const T& value)
    {
        if (size_ < max_size_)
        {
            this->write(value);
        }
        else
        {
            this->insert_or_replace(start_, value);

            end_ = start_;
            start_ = (start_ + 1) % max_size_;
        }
    }

    template <typename T>
    void RingList<T>::remove(std::size_t index)
    {
        if (index == cur_)
        {
            this->next();
        }

        buffer_.erase(index);
    }

    template <typename T>
    void RingList<T>::clear()
    {
        buffer_.clear();
        size_ = 0;
        start_ = end_;
    }

    template <typename T>
    void RingList<T>::insert_or_replace(std::size_t index, const T& value)
    {
        if (buffer_.size() < max_size_)
        {
            auto it = buffer_.begin() + index;
            buffer_.emplace(it, value);
        }
        else
        {
            buffer_[index] = value;
        }
    }
} // namespace util

#endif // RING_LIST_HPP
