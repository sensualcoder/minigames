#include "RingList.hpp"

namespace util
{
    template <>
    class RingList<int>;
    template <>
    class RingList<float>;
    template <>
    class RingList<double>;
} // namespace util
